<?php echo $this->fetch('tab-pills');?>

<?php $panelStyle = ($this->fetch('panelStyle'))?($this->fetch('panelStyle')):('default'); ?>
<div class="row">
	<div class="col-md-12">
		<div class="page-header clearfix">
				<div class="text pull-left">
					<h2><?php echo $this->fetch('pageHeader');?></h2>
					<small><?php echo $this->fetch('pageSubHeader');?></small>
				</div>
		</div>
		<?php if (count($data) == 0) { ?>
			<p class="text-warning">Nenhum registro encontrado!</p>
		<?php } else { ?>
			<div class="box box-<?php echo $panelStyle;?>">
				<div class="box-header">
					<div class="box-title">Cabeçalho</div>
					<div class="box-tools">
						<?php echo $this->fetch('boxTools'); ?>
					</div>
				</div>
				<?php /* <div class="box-body table-responsive"> */ ?>
					<?php //echo $this->fetch('paginator'); ?>
					<table class="table table-hover table-responsive">
						<tbody>
						<?php echo $this->fetch('data-header'); ?>
						<?php echo $this->fetch('data-body'); ?>
						</tbody>
					</table>
					<?php //echo $this->fetch('paginator'); ?>
				<?php /* </div> */ ?>
				<div class="box-footer">
					Rodapé
				</div>
			</div>
		<?php } ?>
	</div>
</div>
